# Arch pratical install guide

## Before the installation

### Create a live usb disk

- Download arch iso from 
[arch download page](https://www.archlinux.org/download/)  

- Validate PGP signature

```sh
gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```  
```sh
pacman-key -v archlinux-version-x86_64.iso.sig
```  
- Create a live disk  
```sh
dd if=archlinux-version-x86_64.iso of=/dev/sdb status="progress"
```  
*Be sure that /dev/sdb is the flash drive you want to install the iso on!*

## On the live disk

### BIOS vs UEFI
- Verify if the computer is running with bios or uefi
```sh
ls /sys/firmware/efi/efivars
```

### Connect to the internet
Use wifi-menu or a cable

### Update system clock
```sh
timedatectl set-ntp true
```

### Partition the disk

To list your device use ```lsblk``` or ```fdisk -l```

To partition your disk use ```fdisk```

#### Partition guideline


| /boot | 200M |
---
| swap  | 150% * RAM |
---
| /     | 15G-25G |
---
| /home | rest |
---

### Format your partition

- /
```sh
mkfs.ext4 /dev/sda2
```
*sda2 is an exemple, it must be the root partition.*


- /home
```sh
mkfs.ext4 /dev/sda4
```
*sda4 is an exemple, it must be the /home partition.*


- /boot
```sh
mkfs.ext4 /dev/sda1
```
*sda1 is an exemple, it must be the /boot partition.*

- swap
``` 
mkswap /dev/sda3
swapon /dev/sda3
```

### Mount your new system

- Create appropriate folders
```sh
mkdir /mnt/boot
mkdir /mnt/home
```

- Mount the partition

```sh
mount /dev/sda1 /mnt/boot
mount /dev/sda4 /mnt/home
mount /dev/sda3 /mnt
```

### Install base packages
```sh
pacstrap /mnt base base-devel vim git
```
### Generate fstab (list of your partition)
```sh
genfstab -U /mnt >> /mnt/etc/fstab
```

### Change root on the new system
```sh
arch-chroot /mnt
```

## On your new installation

### Time zone
```sh
ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

### Localization
- Uncomment the appropriate locales from ```/etc/locale.gen```
- Run ```locale-gen```
- Edit ```/etc/locale.conf``` with ```LANG=en_US.UTF-8```

### LC_*

### Install a network manager

### Set root password
```passwd```

### Install the boot loader

### Exit, umount and reboot
```sh 
exit
umount /mnt -R
shutdown -r now
```

## Get your system ready, post-installation

