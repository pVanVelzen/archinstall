# archInstall

## Files
The installation is separated into 2 different file.

1- The first file `install1.sh` configure the installation before `chroot`.
2- The second file `install2.sh` configure the installation after `chroot`.

The file `locale.gen` as the locale already set. If you want to change the
locale, you can alter this file.

The file  partitionTable.fsdisk contain the partition. If you want to change
the partition, you can alter this file.

