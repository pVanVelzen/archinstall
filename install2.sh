#!/bin/sh

# Time zone
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime

# Localization
cp archinstall/locale.gen /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# Grub
grub-install --target=i386-pc /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

# Computer name
echo "barnabe" > /etc/hostname

#root Password
echo "Setting root password"
passwd

exit
