#!/bin/sh -x
echo "include variables"
if [[ "$1" == "test" ]]; then
  # Since the test environment is vagrant and vagrant require
  # the full path for file in the currently confugure machine.
  source /home/vagrant/archinstall/test-vars.sh
elif [[ "$1" == "prod" ]]; then
  source ./vars.sh
else
  echo "invalid option: prod or test."
fi

touch install.log
echo "Validate bios"
ls /sys/firmware/efi/efivars && echo "MBR" >> install.log || UEFI=uefi && echo "UEFI" >> install.log

echo "Setting system clock"
timedatectl set-ntp true || echo "Setting system clock failed" >> install.log

echo "Partitioning the disk"
echo "The new partition will delete all data on the disk."
sfdisk -d /dev/sda > sda.dump || echo "partition backup failed" >> install.log

# create partition
sfdisk -f /dev/sda < $partitionFile || echo "error partitionning, restoring backup." >> install.log && sfdisk /dev/sda < sda.dump && exit

# Format partition
if [[ -n $UEFI ]]; then
  mkfs.fat -F32 /dev/sda1 || echo "error formating sda1" >> install.log #EFI
else
  mkfs.ext4 /dev/sda1 || echo "error formating sda1" >> install.log #boot
fi

mkfs.ext4 /dev/sda3 || echo "error formating sda3" >> install.log #root
mkfs.ext4 /dev/sda4 || echo "error formating sda4" >> install.log #home

mkswap /dev/sda2 || echo "error mkswap" >> install.log
swapon /dev/sda2 || echo "error swapon" >> install.log

# Mount disk device
mkdir /mnt/boot
mkdir /mnt/home

mount /dev/sda1 /mnt/boot
mount /dev/sda4 /mnt/home
mount /dev/sda3 /mnt

# Install arch
pacstrap /mnt base base-devel vim git networkmanager grub linux linux-firmware lvm2 dhcpcd net-tools wireless_tools dialog wpa_supplicant vi ansible
# gen fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Prepare arch-chroot
cp -r ../archinstall /mnt

# launch second script in new root
arch-chroot /mnt $secondScript || exit

# Umount /mnt after exiting chroot
umount -R /mnt || echo "Unmounting error"

# Reboot 
shutdown -r now

