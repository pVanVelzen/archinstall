#!/bin/sh

# Create user
useradd -m -g wheel puter
passwd puter

# Grafical environnement
pacman -Sy xorg-server xorg-xinit

# Display manager
pacman -S --noconfirm i3-gaps i3status i3blocks dmenu

# Terminal
pacman -S --noconfirm urxvt-unicode

# Browser
pacman -S --noconfirm firefox qutebrowser

# fonts
pacman -S noto-fonts

# network manager
